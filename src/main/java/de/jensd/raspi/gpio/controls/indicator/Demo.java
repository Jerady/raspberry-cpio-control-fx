/* 
 * Copyright 2013 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.raspi.gpio.controls.indicator;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters
 */
public class Demo extends Application {

    @Override
    public void start(Stage primaryStage) {


        final Indicator indicator = new Indicator();
        indicator.setResult(Indicator.Result.INDETERMINDED);
        indicator.setPrefSize(300, 100);

        Button passButton = new Button("PASS");
        passButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                indicator.setPass(Boolean.TRUE);
            }
        });
        Button indetermindedButton = new Button("INDETERMINDED");
        indetermindedButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                indicator.setResult(Indicator.Result.INDETERMINDED);
            }
        });
        Button failButton = new Button("FAIL");
        failButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                indicator.setPass(Boolean.FALSE);
            }
        });
        
        HBox box = new HBox(passButton, indetermindedButton, failButton);
        
        AnchorPane root = new AnchorPane();
        AnchorPane.setTopAnchor(indicator, 10.0);
        AnchorPane.setTopAnchor(box, 230.0);
        
        root.getChildren().addAll(indicator, box);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Indicator Demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX
     * application. main() serves only as fallback in case the
     * application can not be launched through deployment artifacts,
     * e.g., in IDEs with limited FX support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
