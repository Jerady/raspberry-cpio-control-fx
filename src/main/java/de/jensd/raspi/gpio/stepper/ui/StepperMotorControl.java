/*
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.raspi.gpio.stepper.ui;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import de.jensd.raspi.gpio.control.StepperMotorAdapter;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.AnchorPane;

/*
 * @author Jens Deters
 */
public class StepperMotorControl extends AnchorPane {

    private final static Logger LOGGER = Logger.getLogger(StepperMotorControl.class.getName());
    @FXML
    private Button halfBackwardButton;
    @FXML
    private Button halfForwardButton;
    @FXML
    private Button quarterBackwardButton;
    @FXML
    private Button quarterForwardButton;
    @FXML
    private Button stepBackwardButton;
    @FXML
    private Button stepforwardButton;
    @FXML
    private Button backwardButton;
    @FXML
    private Button forwardButton;
    @FXML
    private Button stopButton;
    @FXML
    private ToggleButton connectMotorButton;
    @FXML
    private Button adjustBackwardButton;
    @FXML
    private Button adjustForwardButton;
    @FXML
    private Button exitButton;
    @FXML
    private ToggleButton halfSpeedButton;
    @FXML
    private ToggleButton singleSpeedButton;
    @FXML
    private ToggleButton doubleSpeedButton;
    private StepperMotorAdapter stepperMotorAdapter;

    public StepperMotorControl() {
        init();
    }

    private void init() {
        ResourceBundle resourceBundle = ResourceBundle.
                getBundle("i18n/gpio_stepper");

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/StepperMotorControl.fxml"));
        fxmlLoader.setResources(resourceBundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(StepperMotorControl.class.getName()).log(Level.SEVERE, null, ex);
        }

        AwesomeDude.setIcon(halfBackwardButton, AwesomeIcon.ROTATE_LEFT, "2em");
        AwesomeDude.setIcon(quarterBackwardButton, AwesomeIcon.ROTATE_LEFT, "2em");
        AwesomeDude.setIcon(stepBackwardButton, AwesomeIcon.ROTATE_LEFT, "2em");
        AwesomeDude.setIcon(stepforwardButton, AwesomeIcon.ROTATE_RIGHT, "2em");
        AwesomeDude.setIcon(quarterForwardButton, AwesomeIcon.ROTATE_RIGHT, "2em");
        AwesomeDude.setIcon(halfForwardButton, AwesomeIcon.ROTATE_RIGHT, "2em");
        AwesomeDude.setIcon(backwardButton, AwesomeIcon.BACKWARD, "2em");
        AwesomeDude.setIcon(forwardButton, AwesomeIcon.FORWARD, "2em");
        AwesomeDude.setIcon(stopButton, AwesomeIcon.STOP, "2em");

        AwesomeDude.setIcon(halfSpeedButton, AwesomeIcon.DOUBLE_ANGLE_UP, "2em");
        AwesomeDude.setIcon(singleSpeedButton, AwesomeIcon.DOUBLE_ANGLE_LEFT, "2em");
        AwesomeDude.setIcon(doubleSpeedButton, AwesomeIcon.DOUBLE_ANGLE_DOWN, "2em");

        AwesomeDude.setIcon(adjustBackwardButton, AwesomeIcon.ROTATE_LEFT, "2em");
        AwesomeDude.setIcon(adjustForwardButton, AwesomeIcon.ROTATE_RIGHT, "2em");
        AwesomeDude.setIcon(connectMotorButton, AwesomeIcon.PLAY, "2em");
        AwesomeDude.setIcon(exitButton, AwesomeIcon.OFF, "2em");

        stepperMotorAdapter = new StepperMotorAdapter();

        attachActions();

    }

    @FXML
    public void disconnect() {
        LOGGER.info("disconnect");
        stepperMotorAdapter.disconnect();
    }

    @FXML
    public void connect() {
        LOGGER.info("connect");
        stepperMotorAdapter.connect();
    }

    @FXML
    public void singleStepSequence() {
        LOGGER.info("singleStepSequence");
        stepperMotorAdapter.singleStepSequence();
    }

    @FXML
    public void doubleStepSequence() {
        LOGGER.info("doubleStepSequence");
        stepperMotorAdapter.doubleStepSequence();
    }

    @FXML
    public void halfStepSequence() {
        LOGGER.info("halfStepSequence");
        stepperMotorAdapter.halfStepSequence();
    }

    @FXML
    public void backward() {
        LOGGER.info("backward");
        Platform.runLater(stepperMotorAdapter::backward);
    }

    @FXML
    public void halfRevolutionBackward() {
        LOGGER.info("halfRevolutionForward");
        stepperMotorAdapter.halfRevolutionBackward();
    }

    @FXML
    public void quarterRevolutionBackward() {
        LOGGER.info("halfRevolutionBackward");
        stepperMotorAdapter.quarterRevolutionBackward();
    }

    @FXML
    public void oneStepBackward() {
        LOGGER.info("oneStepBackward");
        stepperMotorAdapter.oneStepBackward();
    }

    @FXML
    public void stop() {
        LOGGER.info("stop");
        Platform.runLater(stepperMotorAdapter::stop);
    }

    @FXML
    public void oneStepForward() {
        LOGGER.info("oneStepForward");
        stepperMotorAdapter.oneStepForward();
    }

    @FXML
    public void quarterRevolutionForward() {
        LOGGER.info("quarterRevolutionForward");
        stepperMotorAdapter.quarterRevolutionForward();
    }

    @FXML
    public void halfRevolutionForward() {
        LOGGER.info("halfRevolutionForward");
        stepperMotorAdapter.halfRevolutionForward();
    }

    @FXML
    public void forward() {
        LOGGER.info("forward");
        Platform.runLater(stepperMotorAdapter::forward);
    }

    @FXML
    public void exitAction() {
        LOGGER.info("");
        Platform.runLater(this::disconnect);
        Platform.exit();
        System.exit(0);
    }

    private void attachActions() {

        connectMotorButton.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
            if (connectMotorButton.isSelected()) {
                AwesomeDude.setIcon(connectMotorButton, AwesomeIcon.EJECT, "2em");
                connect();
            } else {
                AwesomeDude.setIcon(connectMotorButton, AwesomeIcon.PLAY, "2em");
                disconnect();
            }
        });

        adjustBackwardButton.setOnMousePressed((MouseEvent t) -> {
            stepperMotorAdapter.backward();
        });
        adjustBackwardButton.setOnMouseReleased((MouseEvent t) -> {
            stepperMotorAdapter.stop();
        });
        adjustForwardButton.setOnMousePressed((MouseEvent t) -> {
            stepperMotorAdapter.forward();
        });
        adjustForwardButton.setOnMouseReleased((MouseEvent t) -> {
            stepperMotorAdapter.stop();
        });
    }
}
