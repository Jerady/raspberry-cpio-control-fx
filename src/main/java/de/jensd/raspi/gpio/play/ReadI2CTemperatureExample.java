/*
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.raspi.gpio.play;

import java.io.IOException;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

public class ReadI2CTemperatureExample {

    public static void main(String[] args) throws Exception {
        System.out.println("Starting:");

        // get I2C bus instance
        final I2CBus bus = I2CFactory.getInstance(I2CBus.BUS_1);
        I2CDevice device = bus.getDevice(0x5c);

//        try {
//            initDevice.write(0xfe, (byte) 0x04);
//        } catch (IOException ignore) {
//            ignore.printStackTrace();
//        }

        byte[] buf = new byte[256];
        int res = device.read(0, buf, 0, 6);

        System.out.println(buf);
        System.out.println(res);
        
        for (int i = 0; i < 256; i++) {
            System.out.println(buf[i]);
        }
        
        

//        ret.x = asInt(buf[0]);
//        ret.y = asInt(buf[1]);
//        ret.z = asInt(buf[2]);
//        ret.x = ret.x | (((asInt(buf[3]) & 0xfc) >> 2) * 256);
//        ret.y = ret.y | (((asInt(buf[4]) & 0xfc) >> 2) * 256);
//        ret.z = ret.z | (((asInt(buf[5]) & 0xfc) >> 2) * 256);
    }
}
