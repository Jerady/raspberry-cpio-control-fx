/*
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.raspi.gpio.play;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *
 * @author Jens Deters
 */
public class I2Ctest {

    public static void main(String[] args) {

        I2CBus bus;
        I2CDevice device;

        System.out.println("Starting sensors reading:");
        // get I2C bus instance
        try {
            //get i2c bus
            bus = I2CFactory.getInstance(I2CBus.BUS_0);
            System.out.println("Connected to bus OK!");

            //get device itself
            device = bus.getDevice(0x5c);
            System.out.println("Connected to device OK!");

            
            //start sensing, using config registries 6B  and 6C    
            device.write(0x03, (byte) 0b00000000);
            System.out.println("Configuring Device OK!");







        } catch (IOException ex) {
            Logger.getLogger(I2Ctest.class.getName()).log(Level.SEVERE, "ERROR", ex);
        }

    }
}