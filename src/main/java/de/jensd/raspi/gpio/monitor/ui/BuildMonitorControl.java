/*
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.jensd.raspi.gpio.monitor.ui;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import de.jensd.raspi.gpio.control.StepperMotorAdapter;
import de.jensd.raspi.gpio.stepper.ui.StepperMotorControl;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.AnchorPane;

/*
 *
 * @author Jens Deters
 * 
 */
public class BuildMonitorControl extends AnchorPane {

    private final static Logger LOGGER = Logger.getLogger(BuildMonitorControl.class.getName());

    public enum BuildState {

        SUCCESS, RUNNING, FAILED;
    }
    @FXML
    private ToggleButton successButton;
    @FXML
    private ToggleButton runningButton;
    @FXML
    private ToggleButton failedButton;
    @FXML
    private ToggleButton connectMotorButton;
    @FXML
    private Button adjustBackwardButton;
    @FXML
    private Button adjustForwardButton;
    @FXML
    private Button confirmAdjustedButton;
    @FXML
    private Button exitButton;
    private ObjectProperty<BuildState> currentBuildStateProperty;
    private StepperMotorAdapter stepperMotorAdapter;

    public BuildMonitorControl() {
        init();
    }

    private void init() {
        ResourceBundle resourceBundle = ResourceBundle.
                getBundle("i18n/gpio_stepper");

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/BuildMonitorControl.fxml"));
        fxmlLoader.setResources(resourceBundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(StepperMotorControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        stepperMotorAdapter = new StepperMotorAdapter();
        stepperMotorAdapter.doubleStepSequence();

        currentBuildStateProperty = new SimpleObjectProperty<>(BuildState.RUNNING);
        currentBuildStateProperty.addListener(new BuildStateChangeListener());

        AwesomeDude.setIcon(adjustBackwardButton, AwesomeIcon.ROTATE_LEFT, "2em");
        AwesomeDude.setIcon(confirmAdjustedButton, AwesomeIcon.CHECK_SIGN, "2em");
        AwesomeDude.setIcon(adjustForwardButton, AwesomeIcon.ROTATE_RIGHT, "2em");
        AwesomeDude.setIcon(connectMotorButton, AwesomeIcon.PLAY, "2em");
        AwesomeDude.setIcon(exitButton, AwesomeIcon.OFF, "2em");

        AwesomeDude.setIcon(successButton, AwesomeIcon.THUMBS_UP, "4em");
        AwesomeDude.setIcon(runningButton, AwesomeIcon.GEARS, "4em");
        AwesomeDude.setIcon(failedButton, AwesomeIcon.THUMBS_DOWN, "4em");
        attachActions();
    }

    @FXML
    public void disconnect() {
        LOGGER.info("disconnect");
        stepperMotorAdapter.disconnect();
    }

    @FXML
    public void connect() {
        LOGGER.info("connect");
        stepperMotorAdapter.connect();
    }

    @FXML
    public void exitAction() {
        LOGGER.info("");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                disconnect();
            }
        });
        Platform.exit();
        System.exit(0);
    }

    @FXML
    void successAction() {
        LOGGER.info("");
        currentBuildStateProperty.set(BuildState.SUCCESS);
    }

    @FXML
    void runningAction() {
        LOGGER.info("");
        currentBuildStateProperty.set(BuildState.RUNNING);
    }

    @FXML
    void failedAction() {
        LOGGER.info("");
        currentBuildStateProperty.set(BuildState.FAILED);
    }

    @FXML
    void confirmAdjustedAction() {
        LOGGER.info("");
        currentBuildStateProperty.set(BuildState.RUNNING);
    }

    private void attachActions() {

        connectMotorButton.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
            if (connectMotorButton.isSelected()) {
                AwesomeDude.setIcon(connectMotorButton, AwesomeIcon.EJECT, "2em");
                connect();
            } else {
                AwesomeDude.setIcon(connectMotorButton, AwesomeIcon.PLAY, "2em");
                disconnect();
            }
        });


        exitButton.setOnTouchPressed((TouchEvent t) -> {
            exitAction();
        });


        adjustBackwardButton.setOnMousePressed((MouseEvent t) -> {
            stepperMotorAdapter.backward();
        });
        adjustBackwardButton.setOnMouseReleased((MouseEvent t) -> {
            stepperMotorAdapter.stop();
        });
        adjustForwardButton.setOnMousePressed((MouseEvent t) -> {
            stepperMotorAdapter.forward();
        });
        adjustForwardButton.setOnMouseReleased((MouseEvent t) -> {
            stepperMotorAdapter.stop();
        });
    }

    private class BuildStateChangeListener implements ChangeListener<BuildState> {

        @Override
        public void changed(ObservableValue<? extends BuildState> ov, BuildState oldState, BuildState newState) {
            if (BuildState.RUNNING.equals(oldState) && BuildState.SUCCESS.equals(newState)) {
                stepperMotorAdapter.quarterRevolutionBackward();
            } else if (BuildState.RUNNING.equals(oldState) && BuildState.FAILED.equals(newState)) {
                stepperMotorAdapter.quarterRevolutionForward();
            } else if (BuildState.FAILED.equals(oldState) && BuildState.RUNNING.equals(newState)) {
                stepperMotorAdapter.quarterRevolutionBackward();
            } else if (BuildState.SUCCESS.equals(oldState) && BuildState.RUNNING.equals(newState)) {
                stepperMotorAdapter.quarterRevolutionForward();
            } else if (BuildState.SUCCESS.equals(oldState) && BuildState.FAILED.equals(newState)) {
                stepperMotorAdapter.halfRevolutionForward();
            } else if (BuildState.FAILED.equals(oldState) && BuildState.SUCCESS.equals(newState)) {
                stepperMotorAdapter.halfRevolutionBackward();
            }

        }
    };
}
