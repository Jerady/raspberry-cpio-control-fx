/*
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.raspi.gpio.control.ui;

import com.pi4j.io.gpio.PinMode;
import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import de.jensd.raspi.gpio.control.GpioAdapter;
import de.jensd.raspi.gpio.controls.indicator.Indicator;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

/*
 *
 * @author Jens Deters
 */
public class GpioControl extends AnchorPane {

    private final static Logger LOGGER = Logger.getLogger(GpioAdapter.class.getName());
    @FXML
    private GridPane buttonGridPane;
    @FXML
    private ToggleButton toogleGPIO0;
    @FXML
    private ToggleButton toogleGPIO1;
    @FXML
    private ToggleButton toogleGPIO2;
    @FXML
    private ToggleButton toogleGPIO3;
    @FXML
    private ToggleButton toogleGPIO4;
    @FXML
    private ToggleButton toogleGPIO5;
    @FXML
    private ToggleButton toogleGPIO6;
    @FXML
    private ToggleButton toogleGPIO7;
    @FXML
    private ToggleButton toggleModeGPIO0;
    @FXML
    private ToggleButton toggleModeGPIO1;
    @FXML
    private ToggleButton toggleModeGPIO2;
    @FXML
    private ToggleButton toggleModeGPIO3;
    @FXML
    private ToggleButton toggleModeGPIO4;
    @FXML
    private ToggleButton toggleModeGPIO5;
    @FXML
    private ToggleButton toggleModeGPIO6;
    @FXML
    private ToggleButton toggleModeGPIO7;
    @FXML
    private Button exitButton;
    private GpioAdapter gpioAdapter;
    private Indicator indicatorGPIO0;
    private Indicator indicatorGPIO1;
    private Indicator indicatorGPIO2;
    private Indicator indicatorGPIO3;
    private Indicator indicatorGPIO4;
    private Indicator indicatorGPIO5;
    private Indicator indicatorGPIO6;
    private Indicator indicatorGPIO7;

    public GpioControl() {
        init();
    }

    private void init() {

        ResourceBundle resourceBundle = ResourceBundle.
                getBundle("i18n/gpio_control");

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/GpioControl.fxml"));
        fxmlLoader.setResources(resourceBundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        gpioAdapter = new GpioAdapter();

        indicatorGPIO0 = createIndicator();
        indicatorGPIO1 = createIndicator();
        indicatorGPIO2 = createIndicator();
        indicatorGPIO3 = createIndicator();
        indicatorGPIO4 = createIndicator();
        indicatorGPIO5 = createIndicator();
        indicatorGPIO6 = createIndicator();
        indicatorGPIO7 = createIndicator();

        buttonGridPane
                .add(indicatorGPIO0, 0, 1);
        buttonGridPane.add(indicatorGPIO1, 1, 1);
        buttonGridPane.add(indicatorGPIO2, 2, 1);
        buttonGridPane.add(indicatorGPIO3, 3, 1);
        buttonGridPane.add(indicatorGPIO4, 4, 1);
        buttonGridPane.add(indicatorGPIO5, 5, 1);
        buttonGridPane.add(indicatorGPIO6, 6, 1);
        buttonGridPane.add(indicatorGPIO7, 7, 1);

        indicatorGPIO0.passProperty().
                bindBidirectional(gpioAdapter.gpio0StateProperty());
        indicatorGPIO1.passProperty().
                bindBidirectional(gpioAdapter.gpio1StateProperty());
        indicatorGPIO2.passProperty().
                bindBidirectional(gpioAdapter.gpio2StateProperty());
        indicatorGPIO3.passProperty().
                bindBidirectional(gpioAdapter.gpio3StateProperty());
        indicatorGPIO4.passProperty().
                bindBidirectional(gpioAdapter.gpio4StateProperty());
        indicatorGPIO5.passProperty().
                bindBidirectional(gpioAdapter.gpio5StateProperty());
        indicatorGPIO6.passProperty().
                bindBidirectional(gpioAdapter.gpio6StateProperty());
        indicatorGPIO7.passProperty().
                bindBidirectional(gpioAdapter.gpio7StateProperty());

        gpioAdapter.gpio1ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO1));
        gpioAdapter.gpio2ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO2));
        gpioAdapter.gpio3ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO3));
        gpioAdapter.gpio4ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO4));
        gpioAdapter.gpio5ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO5));
        gpioAdapter.gpio6ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO6));
        gpioAdapter.gpio7ModeProperty().addListener(new IOModeChangeEventHandler(toggleModeGPIO7));

        toggleModeGPIO0.setOnAction(new ToggleModeEventHandler(toggleModeGPIO0, 0));
        gpioAdapter.gpio0ModeProperty()
                .addListener(new IOModeChangeEventHandler(toggleModeGPIO0));

        toogleGPIO0.selectedProperty().
                bindBidirectional(gpioAdapter.gpio0StateProperty());
        toogleGPIO0.visibleProperty().
                bind(toggleModeGPIO0.selectedProperty().
                        not());

        toggleModeGPIO1.setOnAction(new ToggleModeEventHandler(toggleModeGPIO1, 1));
        toggleModeGPIO2.setOnAction(new ToggleModeEventHandler(toggleModeGPIO2, 2));
        toggleModeGPIO3.setOnAction(new ToggleModeEventHandler(toggleModeGPIO3, 3));
        toggleModeGPIO4.setOnAction(new ToggleModeEventHandler(toggleModeGPIO4, 4));
        toggleModeGPIO5.setOnAction(new ToggleModeEventHandler(toggleModeGPIO5, 5));
        toggleModeGPIO6.setOnAction(new ToggleModeEventHandler(toggleModeGPIO6, 6));
        toggleModeGPIO7.setOnAction(new ToggleModeEventHandler(toggleModeGPIO7, 7));

        toogleGPIO1.visibleProperty().
                bind(toggleModeGPIO1.selectedProperty().
                        not());
        toogleGPIO2.visibleProperty().
                bind(toggleModeGPIO2.selectedProperty().
                        not());
        toogleGPIO3.visibleProperty().
                bind(toggleModeGPIO3.selectedProperty().
                        not());
        toogleGPIO4.visibleProperty().
                bind(toggleModeGPIO4.selectedProperty().
                        not());
        toogleGPIO5.visibleProperty().
                bind(toggleModeGPIO5.selectedProperty().
                        not());
        toogleGPIO6.visibleProperty().
                bind(toggleModeGPIO6.selectedProperty().
                        not());
        toogleGPIO7.visibleProperty().
                bind(toggleModeGPIO7.selectedProperty().
                        not());

        toogleGPIO1.selectedProperty().
                bindBidirectional(gpioAdapter.gpio1StateProperty());
        toogleGPIO2.selectedProperty().
                bindBidirectional(gpioAdapter.gpio2StateProperty());
        toogleGPIO3.selectedProperty().
                bindBidirectional(gpioAdapter.gpio3StateProperty());
        toogleGPIO4.selectedProperty().
                bindBidirectional(gpioAdapter.gpio4StateProperty());
        toogleGPIO5.selectedProperty().
                bindBidirectional(gpioAdapter.gpio5StateProperty());
        toogleGPIO6.selectedProperty().
                bindBidirectional(gpioAdapter.gpio6StateProperty());
        toogleGPIO7.selectedProperty().
                bindBidirectional(gpioAdapter.gpio7StateProperty());

        toogleGPIO0.setText("");
        toogleGPIO1.setText("");
        toogleGPIO2.setText("");
        toogleGPIO3.setText("");
        toogleGPIO4.setText("");
        toogleGPIO5.setText("");
        toogleGPIO6.setText("");
        toogleGPIO7.setText("");

        AwesomeDude.setIcon(toogleGPIO0, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO1, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO2, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO3, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO4, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO5, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO6, AwesomeIcon.CIRCLE, "2em");
        AwesomeDude.setIcon(toogleGPIO7, AwesomeIcon.CIRCLE, "2em");

        AwesomeDude.setIcon(exitButton, AwesomeIcon.OFF, "2em");

        resetAction();
    }

    private Indicator createIndicator() {
        Indicator indicator = new Indicator();
        indicator.setResult(Indicator.Result.FAIL);
        indicator.setPrefSize(100, 100);
        
        return indicator;
    }

    /*
     * -------------------------- ACTIONS -------------------------- 
     */
    @FXML
    public void initAction() {
        gpioAdapter.test(100);
    }

    @FXML
    public void testAction() {
        LOGGER.info("");
        gpioAdapter.test(1000);
    }

    @FXML
    public void resetAction() {
        LOGGER.info("");
        toggleModeGPIO0.setSelected(false);
        toggleModeGPIO1.setSelected(false);
        toggleModeGPIO2.setSelected(false);
        toggleModeGPIO3.setSelected(false);
        toggleModeGPIO4.setSelected(false);
        toggleModeGPIO5.setSelected(false);
        toggleModeGPIO6.setSelected(false);
        toggleModeGPIO7.setSelected(false);

        gpioAdapter.reset();
    }

    @FXML
    public void gpioDisconnectAction() {
        LOGGER.info("");
        gpioAdapter.disconnect();
    }

    @FXML
    public void gpioConnectAction() {
        LOGGER.info("");
        gpioAdapter.connect();
    }

    @FXML
    public void exitAction() {
        LOGGER.info("");
        Platform.exit();
        System.exit(0);
    }

    private class ToggleModeEventHandler implements EventHandler<ActionEvent> {

        private final ToggleButton button;
        private final int pinNumber;

        public ToggleModeEventHandler(final ToggleButton button, final int pinNumber) {
            this.button = button;
            this.pinNumber = pinNumber;
        }

        @Override
        public void handle(ActionEvent t) {
            if (button.isSelected()) {
                LOGGER.log(Level.INFO, "set Pin: {0} Mode: {1}", new Object[]{pinNumber, PinMode.DIGITAL_INPUT});
                gpioAdapter.setGpioMode(pinNumber, PinMode.DIGITAL_INPUT);
            } else {
                LOGGER.log(Level.INFO, "set Pin: {0} Mode: {1}", new Object[]{pinNumber, PinMode.DIGITAL_OUTPUT});
                gpioAdapter.setGpioMode(pinNumber, PinMode.DIGITAL_OUTPUT);
            }
        }
    }

    private class IOModeChangeEventHandler implements ChangeListener<PinMode> {

        private final ToggleButton modeToggleButton;
        private final Label inLabel;
        private final Label outLabel;

        public IOModeChangeEventHandler(final ToggleButton modeToggleButton) {
            this.modeToggleButton = modeToggleButton;
            inLabel = AwesomeDude.createIconLabel(AwesomeIcon.ARROW_UP, "2.5em");
            outLabel = AwesomeDude.createIconLabel(AwesomeIcon.ARROW_DOWN, "2.5em");
            modeToggleButton.setText("");
            modeToggleButton.setGraphic(inLabel);
        }

        @Override
        public void changed(ObservableValue<? extends PinMode> ov, PinMode t, PinMode newMode) {
            if (newMode.equals(PinMode.DIGITAL_OUTPUT)) {
                modeToggleButton.setGraphic(inLabel);
            } else {
                modeToggleButton.setGraphic(outLabel);
            }
        }
    }
}
