/*
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.raspi.gpio.control;

import com.pi4j.component.motor.impl.GpioStepperMotorComponent;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/*
 *
 * @author Jens Deters
 */
public class StepperMotorAdapter {

    private final static Logger LOGGER = Logger.getLogger(StepperMotorAdapter.class.getName());
    private GpioController gpio;
    private GpioStepperMotorComponent motor;
    private byte[] currentStepSequence;
    private byte[] singleStepSequence;
    private byte[] doubleStepSequence;
    private byte[] halfStepSequence;
    private final int oneRevolution = 2048;
    private final int quarterRevolution = oneRevolution / 4;
    private final int halfRevolution = oneRevolution / 2;
    private final int oneDegreeRevolution = oneRevolution / 360;
    private BooleanProperty connectedProperty;

    public StepperMotorAdapter() {
        init();
    }

    private void init() {

        connectedProperty = new SimpleBooleanProperty(Boolean.FALSE);

        singleStepSequence = new byte[4];
        singleStepSequence[0] = (byte) 0b0001;
        singleStepSequence[1] = (byte) 0b0010;
        singleStepSequence[2] = (byte) 0b0100;
        singleStepSequence[3] = (byte) 0b1000;

        doubleStepSequence = new byte[4];
        doubleStepSequence[0] = (byte) 0b0011;
        doubleStepSequence[1] = (byte) 0b0110;
        doubleStepSequence[2] = (byte) 0b1100;
        doubleStepSequence[3] = (byte) 0b1001;

        halfStepSequence = new byte[8];
        halfStepSequence[0] = (byte) 0b0001;
        halfStepSequence[1] = (byte) 0b0011;
        halfStepSequence[2] = (byte) 0b0010;
        halfStepSequence[3] = (byte) 0b0110;
        halfStepSequence[4] = (byte) 0b0100;
        halfStepSequence[5] = (byte) 0b1100;
        halfStepSequence[6] = (byte) 0b1000;
        halfStepSequence[7] = (byte) 0b1001;

        currentStepSequence = singleStepSequence;
    }

    public void connect() {
        LOGGER.info("connect...");
        gpio = GpioFactory.getInstance();
        final GpioPinDigitalOutput[] pins = {
            gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, PinState.LOW),
            gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, PinState.LOW),
            gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, PinState.LOW),
            gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, PinState.LOW)};

        gpio.setShutdownOptions(true, PinState.LOW, PinPullResistance.PULL_DOWN, pins);

        motor = new GpioStepperMotorComponent(pins);
        motor.setStepSequence(currentStepSequence);
        motor.setStepsPerRevolution(oneRevolution);

        motor.setStepInterval(2);

        connectedProperty.setValue(Boolean.TRUE);
    }

    public void disconnect() {
        LOGGER.info("disconnect.");
        if (gpio != null) {
            gpio.shutdown();
        }
    }

    public boolean isConnected() {
        return connectedProperty.getValue();
    }

    public void singleStepSequence() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.setStepSequence(singleStepSequence);
        currentStepSequence = singleStepSequence;
    }

    public void doubleStepSequence() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.setStepSequence(doubleStepSequence);
        currentStepSequence = doubleStepSequence;
    }

    public void halfStepSequence() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.setStepSequence(halfStepSequence);
        currentStepSequence = halfStepSequence;
    }

    public void oneStepBackward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.step(oneDegreeRevolution);
    }

    public void stop() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.stop();
    }

    public void forward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.reverse();
    }

    public void backward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.forward();
    }

    public void oneStepForward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.step(-oneDegreeRevolution);
    }

    public void halfRevolutionBackward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.step(halfRevolution);
    }

    public void halfRevolutionForward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.step(-halfRevolution);
    }

    public void quarterRevolutionBackward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.step(quarterRevolution);
    }

    public void quarterRevolutionForward() {
        if (!isConnected()) {
            return;
        }
        LOGGER.info("");
        motor.step(-quarterRevolution);
    }
}
